import os.path

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.style as style
style.use('tableau-colorblind10')
import time
import datetime
import pickle
import seqHPC
import torch

rmse = lambda x1, x2, axis=2: np.sqrt(((x1 - x2) ** 2).mean(axis=axis))

# use sklearn r2 instead
# def rsquare(y_true, y_pred):
#     y_bar = y_true.mean()
#     ss_res = ((y_true-y_pred)**2).sum()
#     ss_tot = ((y_true-y_bar)**2).sum()
#     return 1 - (ss_res/ss_tot)

def softmax_grad(softmax):
    # Reshape the 1-d softmax to 2-d so that np.dot will do the matrix multiplication
    s = softmax.reshape(-1,1)
    return torch.diagflat(s) - torch.dot(s, s.T)

def check_seq_error(predicted, seq):
    min_dim = predicted.shape[0]//2
    # the network makes a sequence completion error if the lower part of its output is closer to the lower part of the wrong choice than the lower part of the right choice
    return rmse(predicted[min_dim:], seq[0,min_dim:], axis=None) < rmse(predicted[min_dim:], seq[1,min_dim:], axis=None)

def check_pat_error(predicted, current_context, other_context):
    max_dim = predicted.shape[0]//2
    # the network makes a pattern completion error if the higher part of its output is closer to the higher part from the other context than the higher part from the current context
    return rmse(predicted[:max_dim], other_context[0,:max_dim], axis=None) < rmse(predicted[:max_dim], current_context[1,:max_dim], axis=None)


def sparsify(inp, active_ratio, top_to_one=False, bottom_to_zero=True):

    # Remove batch dimension
    if len(inp.shape)==2:
        inp = inp[0]
    out = np.copy(inp)

    # Set threshold to activity of the neuron that is less active than active_ratio of all neurons
    thr = np.sort(inp)[::-1][int(len(out)*active_ratio)]

    if top_to_one:
        out[inp>thr] = 1
    if bottom_to_zero:
        out[inp<=thr] = 0

    return np.array([out])

def encode_to_thetagamma(x, n_gammas=7):
    x = np.repeat(x[:,np.newaxis,:,:], x.shape[1]-n_gammas+1, axis=1)
    for i in range(1,x.shape[1]):
        x[:,i,:,:] = np.roll(x[:,i,:,:], -i, axis=1)
    return x[:,:,:n_gammas,:]

def decode_from_thetagamma(x):
    return np.concatenate((x[:,:,0,:], x[:,-1,1:,:]), axis=1)

def N_scale_to_N(N_scale):
    return {k:int(v*N_scale) for k,v in seqHPC.constants.NEURON_COUNTS.items()}

def formatted_time(timestamp=None):
    if timestamp is None:
        timestamp = time.time()
    format_string = '%Y_%m_%d_%Hh%Mm%Ss'
    formatted_time = datetime.datetime.fromtimestamp(timestamp).strftime(format_string)
    return formatted_time

def save_results(path, results):
    pickle.dump(results, open(path+"/results.pickle", "wb"))

def flatten_dict(d):
    new_d = {}
    for k,v in d.items():
        if type(v) is dict:
            for k_v, v_v in v.items():
                new_d["_".join([k_v,k])] = v_v
        else:
            new_d[k] = v

    return new_d

def make_fig(fig, ax=None, dir_name="./", name=None, show=False, save_png=True, save_pdf=True, close=True):


    assert ax is not None or name is not None
    if name is None:
        name = "_".join([str(ax.xaxis.get_label().get_text()), str(ax.yaxis.get_label().get_text())])
    dir_name = dir_name if dir_name[-1] == "/" else dir_name+"/"
    if not os.path.isdir(dir_name):
        os.mkdir(dir_name)
        print(dir_name)
    if save_pdf:
        print("Saving pdf figure to", dir_name+name+".pdf")
        fig.savefig(dir_name+name+".pdf", transparent=True, bbox_inches='tight')
    if save_png:
        print("Saving png figure to", dir_name+name+".png")
        fig.savefig(dir_name+name+".png", transparent=False, bbox_inches='tight')
    if show:
        plt.show()
    if close:
        plt.close()

def create_intrins_seq(n_patterns, dim, mu=.2):
    intrins_seq = []
    for i in range(n_patterns):
        pattern = np.zeros(dim)
        elements = np.where(pattern == 0)[0]
        ones = np.random.choice(elements, size=int(dim * mu), replace=False)
        pattern[ones] = 1
        intrins_seq.append(pattern)
    return np.array(intrins_seq)

def walks_to_onehot(walks, env):
    pos = np.array([[step[0]['id'] for step in walk] for walk in walks])
    return np.eye(len(env.locations))[pos]

def walks_to_location_vectors(walks, env):
    pos = np.array([[step[0]['id'] for step in walk] for walk in walks])
    return env.location_vectors[pos]

def decode_position(neurons):
    """t_start and t_end allow you to pick the poritions of the saved data to train on.
    Returns a list of times and decoded positions"""
    #Get testing data
    t = np.array(neurons.history['t'])
    if t_start is None: i_start = 0
    else: i_start = np.argmin(np.abs(t-t_start))
    if t_end is None: i_end = -1
    else: i_end = np.argmin(np.abs(t-t_end))
    t = t[i_start:i_end]
    fr = np.array(neurons.history['firingrate'])[i_start:i_end]
    #decode position from the data and using the decoder saved in the  Neurons class
    decoded_position_GP = neurons.decoding_model_GP.predict(fr)
    return (t, decoded_position_GP)