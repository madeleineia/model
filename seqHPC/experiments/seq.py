import torch.nn
import torch



import seqHPC
from seqHPC import utils, constants
from sklearn.metrics import r2_score
from seqHPC import Experiment

import numpy as np
from scipy.stats import pearsonr
import matplotlib.pyplot as plt
from reservoirpy.observables import rmse, nrmse
from reservoirpy.utils.random import noise

import matplotlib.pyplot as plt
import matplotlib as mpl
import scienceplots
mpl.rcParams.update(mpl.rcParamsDefault)
plt.style.use(['nature'])

import scipy

class SequenceExperiment(Experiment):

    def set_default_parameters(self):
        super().set_default_parameters()
        self.add_parameter('cue_noise', 0, comment="Amount of noise added to the recall cue")
        self.add_parameter('n_pretrain_epochs', 0, comment="Number of pretraining epochs")

        # self.add_parameter('n_pretrain_tasks', 1000, comment="")
        self.add_parameter('n_pretrain_tasks', 0, comment="")
        self.add_parameter('pretrain_task_length', 1, comment="")
        self.add_parameter('n_test_tasks', 3, comment='')
        self.add_parameter('test_task_length', 10, comment='')

        self.add_parameter('recall_autonomous_from', 5, comment='Number of steps in each walk')


    def setup_exploration(self):

        self.config["plot"].set_default_value(True)

    @staticmethod
    def compute_metrics(results, config):

        recalled_data = results["recalled_data"][:,config["recall_autonomous_from"]:,:].reshape(-1, results["recalled_data"][:,config["recall_autonomous_from"]:,:].shape[-1])
        test_sequences = results["test_sequences"][:,config["recall_autonomous_from"]:,:].reshape(-1, results["test_sequences"][:,config["recall_autonomous_from"]:,:].shape[-1])

        return {
            "loss": config["loss_fn"]()(torch.from_numpy(recalled_data).float(), torch.from_numpy(test_sequences).float()).item(),
            "perf": r2_score(test_sequences, recalled_data),
        }

    @staticmethod
    def simulate(path, sim_config):

        print(sim_config["Ns"])

        model = getattr(seqHPC, sim_config["model_cls"])(sim_config)

        pretrain_sequences = sim_config["data"]["pretrain"]["inp"][:sim_config["n_pretrain_tasks"]]
        test_sequences = sim_config["data"]["test"]["inp"][:sim_config["n_test_tasks"]]

        pretrain_shape = pretrain_sequences.shape
        test_shape = test_sequences.shape

        # prepare data for semantic encoding
        pretrain_sequences = pretrain_sequences.reshape(-1, 3, sim_config["dataset_obj"].height,  sim_config["dataset_obj"].width)
        test_sequences = test_sequences.reshape(-1, 3, sim_config["dataset_obj"].height,  sim_config["dataset_obj"].width)
        
        # semantic encoding
        encoded_test_sequences = model.semantic_ae.encoder(torch.from_numpy(test_sequences).float()).detach().numpy()
        encoded_test_sequences = encoded_test_sequences.reshape(sim_config["n_test_tasks"], sim_config["test_task_length"], -1)


        # prepare "emotion" values
        episodic_encoding_rate = np.array([
            [1,1,1,1,1,1,1,1,1,1],
            [1,1,1,1,1,1,1,1,1,1],
            [1,1,1,1,1,1,1,1,1,1],
        ])

        # episodic encoding
        model.encode_walks(encoded_test_sequences, mode="encoding", autonomous_from=np.inf, episodic_encoding_rate=episodic_encoding_rate)

        # prepare cue (add noise)
        recall_cues = np.copy(encoded_test_sequences)
        recall_cues = recall_cues + noise(
            dist="normal",
            shape=recall_cues.shape,
            gain=sim_config["cue_noise"],
            rng=sim_config["rng"])

        # recall from cue
        recalled_data = model.recall_walks(recall_cues, autonomous_from=sim_config["recall_autonomous_from"])
        recalled_data = recalled_data["out"]

        # prepare data for semantic decoding
        recall_cues = torch.from_numpy(recall_cues.reshape(sim_config["n_test_tasks"] * sim_config["test_task_length"], -1, 1, 3)).float()
        recalled_data = torch.from_numpy(recalled_data.reshape(sim_config["n_test_tasks"] * sim_config["test_task_length"], -1, 1, 3)).float()

        # Semantic decoding
        recall_cues = model.semantic_ae.decoder(recall_cues).detach().numpy()
        recalled_data = model.semantic_ae.decoder(recalled_data).detach().numpy()

        recall_cues = recall_cues.reshape(sim_config["n_test_tasks"], sim_config["test_task_length"], *recall_cues.shape[1:])
        recalled_data = recalled_data.reshape(sim_config["n_test_tasks"], sim_config["test_task_length"], *recalled_data.shape[1:])

        test_sequences = test_sequences.reshape(sim_config["n_test_tasks"], sim_config["test_task_length"], 3, sim_config["dataset_obj"].height,  sim_config["dataset_obj"].width)


        if sim_config["plot"]:

            for task in range(recalled_data.shape[0]):
                fig, axs = plt.subplots(3, recalled_data.shape[1], figsize=(20,80))
                for t in range(recalled_data.shape[1]):
                    axs[0,t].imshow(np.moveaxis(test_sequences[task,t], 0, -1))
                    axs[1,t].imshow(np.moveaxis(recall_cues[task,t], 0, -1))
                    axs[2,t].imshow(np.moveaxis(recalled_data[task,t], 0, -1))

                plt.tight_layout()
                plt.show()

        results = {
            "recalled_data": recalled_data,
            "test_sequences": test_sequences
        }

        print(SequenceExperiment.compute_metrics(results, sim_config))

        seqHPC.utils.save_results(path, results)
        return results