import numpy as np
import torch
import reservoirpy as rpy

class Model():

    def __init__(self, config):

        super().__init__()
        self.config = config
        self.set_seed()
        self.create()

    @property
    def rng(self):
        return self.config["rng"]

    def forward(self, x):
        raise NotImplementedError("Subclasses should implement this!")

    def set_seed(self):
        np.random.seed(self.rng.integers(2 ** 32))
        self.set_framework_seeds()

    def set_framework_seeds(self):
        rpy.set_seed(self.config["seed"])
        torch.manual_seed(self.rng.integers(2 ** 32))

    def reset_memories(self, requires_grad):
        raise NotImplementedError("Subclasses should implement this!")



    def create(self):
        raise NotImplementedError("Subclasses should implement this!")

    def encode_prepare(self, data, contexts):
        return data, contexts

    def encode_walks(self, walks, mode, slow_plasticity=None, autonomous_from=np.inf, contexts=None, episodic_encoding_rate=None, **kwargs):

        assert mode in ["pretraining", "encoding", "recall"]

        if slow_plasticity is None:
            slow_plasticity = mode in ["pretraining"]
        fast_plasticity = mode == "encoding"

        walks, contexts = self.encode_prepare(walks, contexts)
        outputs = []


        for i in range(len(walks)):
            if mode=="recall":
                out = self.recall_walk(walks[i], autonomous_from=autonomous_from,
                                       context=None if contexts is None else contexts[i], **kwargs)
            else:
                out = self.encode_walk(walks[i], slow_plasticity, fast_plasticity, autonomous_from=autonomous_from,
                                       context=None if contexts is None else contexts[i], episodic_encoding_rate=episodic_encoding_rate[i] if episodic_encoding_rate is not None else None, **kwargs)
            outputs.append(out)

        if type(outputs[0])==dict:
            return {k:np.array([out[k] for out in outputs]) for k in outputs[0].keys()}
        else:
            return np.array(outputs)

    def recall_walks(self, walks, autonomous_from=np.inf, contexts=None, **kwargs):
        return self.encode_walks(walks, mode="recall", autonomous_from=autonomous_from, contexts=contexts, **kwargs)

    def encode_walk(self, walk, slow_plasticity, fast_plasticity, autonomous_from=np.inf, starting_idx=0, context=None, **kwargs):
        raise NotImplementedError("Subclasses should implement this!")

    def recall_walk(self, walk, autonomous_from=np.inf, starting_idx=0, context=None, **kwargs):
        return self.encode_walk(walk, slow_plasticity=False, fast_plasticity=False, autonomous_from=autonomous_from, starting_idx=starting_idx, context=context, **kwargs)
