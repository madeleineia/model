import reservoirpy as rpy

import torch
from torch.nn import Linear,RNN,ReLU,Sequential,MSELoss,Module
from torch.nn.parameter import Parameter
import torch.nn.functional as F
from torch.optim import Adam

from seqHPC.models.deep import DeepModel
from seqHPC.modules import *
import numpy as np

from seqHPC.models import vgg
from seqHPC.models import resnet


class MadeleineModel(DeepModel):

    def __init__(self, config):
        super().__init__(config)

    def create(self):

        # create semantic net
        if "vgg" in self.config["semantic_arch"]:
            self.semantic_ae = vgg.VGGAutoEncoder(vgg.get_configs(self.config["semantic_arch"]))
        elif "resnet" in self.config["semantic_arch"]:
            self.semantic_ae = resnet.ResNetAutoEncoder(*resnet.get_configs(self.config["semantic_arch"]))

        self.semantic_ae.eval()

        checkpoint = torch.load(self.config["semantic_resume_path"], map_location=torch.device('cpu'))
        

        state_dict = checkpoint['state_dict']
        remove_prefix = 'module.'
        state_dict = {k[len(remove_prefix):] if k.startswith(remove_prefix) else k: v for k, v in state_dict.items()}

        model_dict = self.semantic_ae.state_dict()
        model_dict.update(state_dict)
        self.semantic_ae.load_state_dict(model_dict)

        self.populations = torch.nn.ModuleDict({})

        self.populations.update({

            "DG": ContextLinear(
                self.config["dim"],
                self.config["Ns"]["DG"],
                self.config["n_contexts"],
                self.config["DG_context_weight"] if self.config["n_contexts"] else 0,
                spars_ratio=self.config["spars_ratios"]["DG"],
                activation_func=self.config["DG_activation_func"],
                beta=self.config["DG_beta"],
                bias=False,
                requires_grad=False,
                zeroify=True
            ),

            "DG_CA3": ContextLinear(
                self.config["Ns"]["DG"],
                self.config["dim"],
                self.config["n_contexts"],
                context_weight=0,
                spars_ratio=1,
                activation_func=torch.nn.Identity(),
                bias=False,
                requires_grad=False,
                zeroify=True
            ),

            "CA3_DG": ContextLinear(
                self.config["dim"],
                self.config["Ns"]["DG"],
                self.config["n_contexts"],
                self.config["DG_context_weight"] if self.config["n_contexts"] else 0,
                spars_ratio=1,
                activation_func=torch.nn.Identity(),
                bias=False,
                requires_grad=False,
                zeroify=True
            ),

        })

        self.normalize = lambda input: torch.nn.functional.normalize(input, p=2.0, dim=0, eps=1e-12, out=None)

    def reset_optimizer(self):
        try:

            # params = list(self.parameters())
            #
            # for p in params:
            #     print(p.shape)

            params = self.parameters()
            self.optimizer = Adam(params, lr=self.config["slow_alpha"])
        except ValueError:
            pass

    def change_mode(self, new_mode):
        super()

    def encode_walk(self, walk, slow_plasticity, fast_plasticity, autonomous_from=np.inf, starting_idx=None, context=None, return_activity_from=[], episodic_encoding_rate=None):

        # Set pytorch training mode
        self.train(mode=slow_plasticity)

        SIouts = []

        if return_activity_from:
            activity = {k:[] for k in return_activity_from}

        for t in range(walk.shape[0]):

            if t >= autonomous_from:
                CA3 = DG_to_CA3_recall

            else:
                ECin = walk[t]
                if "ECin" in return_activity_from:
                    activity["ECin"].append(ECin)

                CA3 = ECin

            if "CA3" in return_activity_from:
                activity["CA3"].append(CA3)

            SIout = CA3
            SIouts.append(SIout)



            if t > 0:

                if t < autonomous_from and fast_plasticity:
                    norm_ECin = self.normalize(ECin) if self.config["normalize_fast_layers"] else ECin
                    norm_old_CA3 = self.normalize(old_CA3) if self.config["normalize_fast_layers"] else old_CA3

                    # update keys
                    self.populations["DG"].update("bio_key", eta=self.config["fast_alpha"]*(episodic_encoding_rate[t] if episodic_encoding_rate is not None else 1), device=self.device, x=norm_ECin)
                    self.populations["CA3_DG"].update("bio_key", eta=self.config["fast_alpha"]*(episodic_encoding_rate[t] if episodic_encoding_rate is not None else 1), device=self.device, x=norm_old_CA3)

                    # compute autoassociation
                    DG = self.populations["DG"](norm_ECin, None if context is None else context[t])

                    # update values
                    self.populations["DG_CA3"].update("bio_value", eta=self.config["fast_alpha"]*(episodic_encoding_rate[t] if episodic_encoding_rate is not None else 1), x=DG, t=ECin)

                    self.populations["DG"].update_count += 1
                    self.populations["DG_CA3"].update_count += 1
                    self.populations["CA3_DG"].update_count += 1

                if t >= autonomous_from-1:

                    norm_CA3 = self.normalize(CA3) if self.config["normalize_fast_layers"] else CA3

                    # {'loss': 0.00011369091225787997, 'perf': 0.9979167896777805}
                    # {'loss': 0.00010603608097881079, 'perf': 0.9980413199090132}
                    # {'loss': 0.00010444347572047263, 'perf': 0.9981251936502856}

                    # delta

                    # compute heteroassociation
                    CA3_to_DG_recall = self.populations["CA3_DG"](norm_CA3, None if context is None else context[t])
                    DG_recall = self.populations["DG"](CA3_to_DG_recall, current_input=True, context=None if context is None else context[t])
                    norm_DG_recall = self.normalize(DG_recall) if self.config["normalize_fast_layers"] else DG_recall
                    DG_to_CA3_recall = self.populations["DG_CA3"](norm_DG_recall, None if context is None else context[t])

            old_CA3 = CA3

        SIouts = torch.stack(SIouts)
        ret = {"out": SIouts.detach().cpu().numpy(), }

        for k in return_activity_from:
            ret[k] = torch.stack(activity[k]).detach().cpu().numpy()


        return ret