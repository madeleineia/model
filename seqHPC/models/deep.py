import torch
from torch.optim import Adam
from torch.nn import Module, BCELoss


import numpy as np

from seqHPC import Model

class DeepModel(Model, Module):

    def __init__(self, config, input_size=100, hidden_size=1000, num_layers=1, output_size=100, nonlinearity='tanh'):

        self.device = torch.device("cuda:0" if not config["platform"] == "cpu" else "cpu")

        super().__init__(config)

        print("Using", self.device, "device")
        self.to(self.device)

        self.loss_fn = config["loss_fn"]().to(self.device)

        self.reset_optimizer()

    def reset_optimizer(self):
        try:
            self.optimizer = Adam(self.parameters(), lr=self.config["slow_alpha"])
        except ValueError:
            pass

    def encode_prepare(self, walks, contexts):
        self.reset_optimizer()

        if self.device != "cpu":
            torch.cuda.empty_cache()

        return torch.from_numpy(walks).float().to(self.device), (torch.from_numpy(contexts).float().to(self.device) if contexts is not None else None)