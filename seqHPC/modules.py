import numpy as np
import torch
from torch.nn import Linear,RNN,ReLU,Sequential,MSELoss,Module
from torch.nn.parameter import Parameter
import torch.nn.functional as F
from scipy.special import expit as sigmoid


def kWTA(x, spars_ratio, loser_value="AUTO", activation_func=None, pre=True):
    assert loser_value != "AUTO" or activation_func is not None
    if loser_value == "AUTO":
        if activation_func in [torch.relu, F.relu]:
            loser_value = 0
        elif activation_func in [torch.sigmoid, F.sigmoid, torch.softmax, F.softmax, torch.nn.Softmax(dim=0), torch.tanh, F.tanh] or type(activation_func)==torch.nn.Softmax:
            if activation_func in [torch.tanh, F.tanh] and not pre:
                loser_value = -1
            elif activation_func in [torch.sigmoid, F.sigmoid] and not pre:
                loser_value = 0
            else:
                loser_value = torch.finfo().max
        else:
            print("Cannot find activation function")
            assert False
    size = x.shape[0]
    k_kWTA = int(spars_ratio * size)
    topval = x.topk(k_kWTA)[0][-1]
    winners = (x >= topval).to(x)
    losers = (x < topval).to(x)
    x = winners * x - losers * loser_value
    return x


class ContextLinear(Linear):
    def __init__(self, in_features, out_features, n_contexts, context_weight, spars_ratio, activation_func=None, beta=1, bias=True, requires_grad=True, zeroify=False, connectivity=1, neuron_wise_connectivity=True, rng=None, device=None):
        super(ContextLinear, self).__init__(in_features+(n_contexts if context_weight>0 else 0), out_features, bias=bias)
        self.context_weight = context_weight
        self.activation_func = F.relu if activation_func is None else activation_func
        self.spars_ratio = spars_ratio
        self.requires_grad_(requires_grad)
        self.beta = beta

        if zeroify:
            with torch.no_grad():
                self.weight *= 0
                if bias:
                    self.bias *= 0

        self.update_count = 0

        self.connectivity = connectivity
        if connectivity < 1:
            if neuron_wise_connectivity: # outneuron-wise sampling
                mask = rng.choice([0,1], size=(out_features), p=[1-connectivity, connectivity])
                mask = np.tile(mask[:,None], (1,in_features))
            else: # synaptic wise sampling
                mask = rng.choice([0,1], size=(out_features, in_features), p=[1-connectivity, connectivity])
            self.mask = torch.from_numpy(mask).float().to(device)



    def update(self, type, eta=None, remember_rate=None, forget_rate=None, device=None, **tensors):

        with torch.no_grad():

            for k,v in tensors.items():
                tensors[k] = v.detach().clone()

            if type == "delta":
                self.delta_update(tensors["x"], tensors["out"], tensors["t"], eta)
            elif type == "bio_key":
                self.bio_key_update(tensors["x"], eta, device)
            elif type == "manual_key":
                self.manual_key_update(tensors["x"])
            elif type == "bio_value":
                self.bio_value_update(tensors["x"], tensors["t"], eta)
            elif type == "manual_value":
                self.manual_value_update(tensors["t"])
            elif type == "hebbian":
                self.hebbian_update(tensors["x"], remember_rate=remember_rate, forget_rate=forget_rate, t=tensors["t"] if "t" in tensors.keys() else None)
            elif type == "postsynaptic":
                self.postsynaptic_update(tensors["x"], eta=eta, t=tensors["t"] if "t" in tensors.keys() else None)

    def delta_update(self, x, out, t, eta):
        with torch.no_grad():
            self.weight += eta * torch.outer(t - out, x)

    def bio_key_update(self, x, eta, device):
        with torch.no_grad():
            rate = eta * torch.eye(self.weight.shape[0], requires_grad=False, device=device)[self.update_count % self.weight.shape[0]]
            self.weight[:,:] = (1-torch.stack([rate]*self.weight.shape[1]).T) * self.weight + torch.outer(rate, x)

    def manual_key_update(self, x):
        with torch.no_grad():
            self.weight[self.update_count%self.weight.shape[0],:] = x

    def bio_value_update(self, x, t, eta):
        with torch.no_grad():
            self.weight[:,:] = (1 - (
                        eta * torch.stack([x] * self.weight.shape[0]))) * self.weight + eta * torch.outer(
                t, x)

    def manual_value_update(self, t):
        with torch.no_grad():
            self.weight[:,self.update_count%self.weight.shape[1]] = t

    def hebbian_update(self, x, remember_rate, forget_rate, t=None):
        if t is None:
            t = x
        with torch.no_grad():
            self.weight *= forget_rate
            self.weight += remember_rate * torch.outer(t,x)

    def postsynaptic_update(self, x, eta, t=None):
        if t is None:
            t = x
        with torch.no_grad():
            self.weight += eta * torch.tile(t[:,None], (1,self.weight.shape[1])) * (torch.tile(x[None,:], (self.weight.shape[0],1)) - self.weight[:,:])

    def __call__(self, inp, current_input=False, return_inp_current=False, context=None, bypass_kWTA=False, bypass_activation=False):

        if self.connectivity < 1:
            with torch.no_grad():
                self.weight[:,:inp.shape[0]] *= self.mask


        if return_inp_current or current_input:
            if current_input:
                h = inp
            else:
                h = F.linear(inp, self.weight[:,:inp.shape[0]])
            if return_inp_current:
                inp_current = h.detach().clone()

            if self.bias is not None:
                h += self.bias
            if self.context_weight > 0:
                h += F.linear(context*self.context_weight, self.weight[:,inp.shape[0]:])

        else:
            if self.context_weight > 0:
                inp = torch.cat([inp, context*self.context_weight], dim=-1)
            h = super().__call__(inp)

        if not bypass_kWTA and self.spars_ratio < 1:
            h = kWTA(h, self.spars_ratio, activation_func=self.activation_func)

        y = h if bypass_activation else self.activation_func(h*self.beta)

        if return_inp_current:
            return y, inp_current
        else:
            return y

class ContextLeakyLinear(ContextLinear):
    def __init__(self, in_features, out_features, leak, n_contexts, context_weight, device, spars_ratio, activation_func=None, beta=1, bias=True, requires_grad=True):
        super(ContextLeakyLinear, self).__init__(in_features, out_features, n_contexts, context_weight, spars_ratio, activation_func, beta, bias)

        self.leak = leak
        self.initial_state = torch.zeros(out_features).to(device)

    def __call__(self, inp, old_h, current_input=False, return_inp_current=False, context=None):
        if return_inp_current:
            h, inp_current = super().__call__(inp, current_input=current_input, return_inp_current=return_inp_current, context=context, bypass_kWTA=True)
        else:
            h = super().__call__(inp, current_input=current_input, return_inp_current=return_inp_current, context=context, bypass_kWTA=True)
        h = (1-self.leak) * old_h + self.leak * h

        if self.spars_ratio < 1:
            h = kWTA(h, spars_ratio=self.spars_ratio, activation_func=self.activation_func, pre=False)

        if return_inp_current:
            return h, inp_current
        else:
            return h

class ContextRNN(Module):
    def __init__(self, input_size, hidden_size, n_contexts, context_weight, device, spars_ratio, activation_func=torch.tanh):
        super(ContextRNN, self).__init__()
        self.initial_state = torch.zeros(hidden_size).to(device)
        self.context_weight = context_weight


        self.input_layer = Linear(input_size, hidden_size, device=device, bias=False)
        self.recurrent_layer = Linear(hidden_size, hidden_size, device=device, bias=True)
        if context_weight > 0:
            self.context_layer = Linear(n_contexts, hidden_size, device=device, bias=False)

        self.activation_func=activation_func
        self.spars_ratio = spars_ratio

    def __call__(self, inp, old_h, current_input=False, return_inp_current=False, context=None):

        if current_input:
            h = inp
        else:
            h = self.input_layer(inp)


        if return_inp_current:
            inp_current = h.detach().clone()

        h += self.recurrent_layer(old_h) # here is where bias is added and centering is done

        if self.context_weight > 0:
            h += self.context_layer(context*self.context_weight)

        y = self.activation_func(h)
        if self.spars_ratio < 1:
            y = kWTA(y, spars_ratio=self.spars_ratio, activation_func=self.activation_func)

        if return_inp_current:
            return y, inp_current
        else:
            return y