from .moving_mnist import MovingMNISTDataset
from .video import VideoDataset
from .folder_videos import FolderVideoDataset