import numpy as np
import matplotlib.pyplot as plt
from .video import VideoDataset
import torch
import torchvision

class MovingMNISTDataset(VideoDataset):

    input_as_ECin = False
    ECout_as_output = False
    activation_func = torch.nn.Sigmoid()
    loss_fn = torch.nn.BCELoss
    dim = 64*64

    def load_data(self, config):
        dataset = torchvision.datasets.MovingMNIST(
            "seqHPC/datasets/media", 
            download=True
        )
        dataset = self.preprocess(dataset)

        # remove channel dimension and flatten pixels
        dataset.data = dataset.data.reshape(dataset.data.shape[0], dataset.data.shape[1], -1)

        pretrain_data = dataset.data[:config["n_pretrain_tasks"], :config["pretrain_task_length"]]
        test_data = dataset.data[config["n_pretrain_tasks"]:config["n_pretrain_tasks"]+config["n_test_tasks"], :config["test_task_length"]]

        return {
            "pretrain": {
                "inp": pretrain_data.numpy()
            },
            "test": {
                "inp": test_data.numpy()
            }
        }

    def preprocess(self, dataset):
        dataset.data = dataset.data.float()
        dataset.data /= 255
        return dataset
