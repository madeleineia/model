import seqHPC

import numpy as np
import matplotlib.pyplot as plt

class VideoDataset(seqHPC.Dataset):

    def generate_data(self, config):
        return self.load_data(config)
