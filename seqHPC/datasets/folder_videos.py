import numpy as np
import matplotlib.pyplot as plt
from .video import VideoDataset
import torch
import torchvision
import os

class FolderVideoDataset(VideoDataset):

    input_as_ECin = True
    ECout_as_output = True
    activation_func = torch.nn.Sigmoid()
    loss_fn = torch.nn.MSELoss
    
    original_width = 1920
    original_height = 1080

    scaling_factor = 20
    width = original_width // scaling_factor
    height = original_height // scaling_factor
    
    # dim = (width*height*3)
    dim = 512*3

    def load_data(self, config):
        
        MAX_FRAMES = 280
        FRAME_SKIP = 28

        dataset = []
        for f in os.listdir(config["media_path"]):
            print(f)
            dataset.append(torchvision.io.read_video(config["media_path"]+"/"+f, output_format="TCHW")[0][:MAX_FRAMES][::FRAME_SKIP])


        dataset = torch.stack(dataset)
        print(dataset.shape)
        dataset = self.preprocess(dataset)

        # flatten
        dataset = dataset.reshape(dataset.shape[0], dataset.shape[1], -1)

        pretrain_data = dataset[:config["n_pretrain_tasks"], :config["pretrain_task_length"]]
        test_data = dataset[config["n_pretrain_tasks"]:config["n_pretrain_tasks"]+config["n_test_tasks"], :config["test_task_length"]]

        return {
            "pretrain": {
                "inp": pretrain_data.numpy()
            },
            "test": {
                "inp": test_data.numpy()
            }
        }

    def preprocess(self, dataset):
        dataset = dataset.float()
        dataset /= 255

        shape = dataset.shape
        dataset = dataset.reshape(-1, *shape[2:])
        dataset = torchvision.transforms.Resize((self.height,self.width))(dataset)
        dataset = dataset.reshape(*shape[:3], *dataset.shape[-2:])

        return dataset
