import seqHPC
from seqHPC import utils, constants
import torch.nn.functional as F

import os
import shutil
import json
import numpy as np
import pickle
from math import prod
import torch


class Parameter(object):

    def __init__(self, name, default_value, comment=None):
        super(Parameter, self).__init__()
        self.name = name
        self.default_value = default_value
        self.exploration_values = []
        self.comment = comment

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def set_default_value(self, value):
        print("Setting {name} default value to {value}".format(name=self.name, value=value))
        self.default_value = value

    @property
    def default(self):
        return len(self.exploration_values) == 0

class Experiment:

    def __init__(self, args):

        self.config = dict({})
        self.exploration = dict({})
        self.name = type(self).__name__+'_'+utils.formatted_time() if args.experiment_id is None else args.experiment_id
        print(self.name)

        self.set_default_parameters()
        self.set_args_parameters(args)

        # For details: https://www.plafrim.fr/
        if self.config["platform"].default_value=="plafrim":
            from . import plafrim_utils
            self.job_dispatcher = plafrim_utils.JobDispatcher(self.name)

    @property
    def path(self):
        return "logs/"+self.name

    def add_parameter(self, name, default_value, comment=None):
        assert name not in self.config.keys()
        self.config[name] = Parameter(name, default_value, comment)

    def set_default_parameters(self):
        self.add_parameter('bio_spars', 0, comment='Closeness to biological sparsity values')
        self.add_parameter('no_kWTA_in_MSP', True, comment='Bypass kWTA in monosynaptic pathway')
        self.add_parameter('normalize_fast_layers', True, comment='Normalize keys before storing them in MHN')



        self.add_parameter('DG_activation_func', torch.nn.Softmax(dim=0), comment='Activation function for DG. If None then uses default one of module.')
        # self.add_parameter('DG_activation_func', None, comment='Activation function for DG. If None then uses default one of module.')
        # self.add_parameter('CA3_activation_func', torch.nn.Softmax(dim=0), comment='Activation function for CA3. If None then uses default one of module.')
        self.add_parameter('CA3_activation_func', F.relu, comment='Activation function for CA3. If None then uses default one of module.')
        self.add_parameter('DG_beta', 1e6, comment="Beta for DG")
        # self.add_parameter('DG_beta', 1, comment="Beta for DG")
        # self.add_parameter('CA3_beta', 1e6, comment="Beta for CA3")
        self.add_parameter('CA3_beta', 1, comment="Beta for CA3")

        self.add_parameter('CA3_inp_connectivity', 1, comment="Probability of CA3 neuron receiving external input in Levy model")
        self.add_parameter('CA3_CA3_connectivity', 1, comment="Probability of keeping CA3_to_CA3 connection in Levy model")

        self.add_parameter('media_path', "seqHPC/datasets/media/claudia", comment="Path to media files")
        self.add_parameter('semantic_resume_path', "seqHPC/models/zoo/imagenet-vgg16.pth", comment="Path to pretrained semantic model")
        self.add_parameter('semantic_arch', "vgg16", comment="Architecture of semantic network")



        # self.add_parameter('dim', "ECin", comment="Dimension of each pattern. Set to 'ECin' for taking config['Ns']['ECin'] value. WARNING: when loading data, the dimension of the loaded data is used.")
        melchior_ns = [50]#[(3)/seqHPC.constants.MELCHIOR_N_CONVERSION/seqHPC.constants.NEURON_COUNTS["CA3"]]#[200,1000]
        self.add_parameter('N_scale', melchior_ns[0]*seqHPC.constants.MELCHIOR_N_CONVERSION, comment='Scaling of network size (NB: should be multiplied by MELCHIOR_N_CONVERSION to match with N from Melchior et al. (2019))')
        self.add_parameter('N', 500, comment='Number of neurons for models which do not use hippocampal subfields')


        self.add_parameter('msp', True, comment="Whether to use the monosynaptic pathway or not")
        self.add_parameter('encoding_thr', 0, comment="Minimal descrepancy to store new memories")
        self.add_parameter('full_CA3_RNN', False, comment="Whether to use a RNN for CA3")
        self.add_parameter('n_contexts', 0, comment="Number of different contexts")
        self.add_parameter('context_learning', False, comment="Whether to learn context representation")
        self.add_parameter('max_entries', None, comment="Maximum number of memories in explicit models like MHN")
        self.add_parameter('fast_alpha', 1, comment='Fast learning rate')
        self.add_parameter('fast_alpha_recurrence', 1, comment='Fast learning rate for CA3 recurrence in Lisman and Levy models')
        self.add_parameter('remember_rate', 0, comment='Rate of remembering')
        self.add_parameter('forget_rate', 0, comment='Rate of forgetting')
        self.add_parameter('slow_alpha', 2e-4, comment="Slow learning rate")
        self.add_parameter('target_keys_rule', "backprop", comment="Learning rule for target keys")
        self.add_parameter('target_keys_alpha', 1e-4, comment="Learning rate for the target keys")
        # self.add_parameter('target_keys_beta', 1, comment="Softmax gain for leaning target keys")
        self.add_parameter('plot', False, comment="Whether to plot during experiment")

        self.add_parameter('leak', 1, comment="Leak rate")

        # self.add_parameter('dataset_cls', "AgentInABoxGCMultiscale", comment='dataset class')
        # self.add_parameter('data', "AgentInABoxGCMultiscale", comment="Data to use. Set to 'generate' for generating it at the beginning of the simulation.")

    def set_args_parameters(self, args):
        self.add_parameter('args', args, comment='Keep track of all arguments that were passed for reproducibility purposes')
        self.add_parameter('seed', args.seed, comment='Seed for the random number generators')
        self.add_parameter('platform', args.platform, comment='Which computing platform to use. See docs for PlaFRIM at https://www.plafrim.fr/')
        self.add_parameter('model_cls', args.model_cls, comment='Name of the model class')
        self.add_parameter('class_to_run', args.class_to_run, comment='Name of the experiment class')
        self.add_parameter('dataset_cls', args.dataset_cls, comment='dataset class')


    def __call__(self):

        os.makedirs(self.path+"/code")
        shutil.copy("main.py", self.path+"/code")
        shutil.copytree("seqHPC", self.path+"/code"+"/seqHPC", ignore=shutil.ignore_patterns('*.pickle', '*.pyc'))

        self.setup_exploration()
        self.launch_exploration()

    def setup_exploration(self):
        raise NotImplementedError("Subclasses should implement this!")

    def launch_exploration(self):
        default_config = {p for p in self.config.values() if p.default} # config that will not change
        explorer_config = set(self.config.values()) - default_config # config that will change

        # Compute the number of simulations in the exploration
        if len(explorer_config) > 0:
            exploration_length = [len(p.exploration_values) for p in explorer_config]
            assert np.all(np.array(exploration_length) == exploration_length[0]) # check if all explorer parameters have the same number of exploration values
            exploration_length = exploration_length[0]
        else:
            exploration_length = 1

        iterator = range(exploration_length)
        if not self.config["platform"].default_value=="plafrim":
            from tqdm import tqdm
            iterator = tqdm(iterator)
        for i in iterator:
            sim_config = dict({}) # Now get parameters for this simulation
            for p in explorer_config:
                sim_config[p.name] = p.exploration_values[i]
            for p in default_config:
                sim_config[p.name] = p.default_value

            # run this simulation
            self.launch_simulation(sim_config, i)

        # For details: https://www.plafrim.fr/
        if self.config["platform"].default_value=="plafrim":
            self.job_dispatcher.launch_jobs()

    def set_simulation_seed(self, sim_config):
        if sim_config["seed"] is None:
            sim_config["seed"] = 1 + np.random.randint(2**32)
        sim_config["rng"] = np.random.default_rng(sim_config["seed"])
        np.random.seed(sim_config["seed"])
        return sim_config

    def create_dataset(self, sim_config, rng):
        if type(rng) is not np.random.Generator:
            rng = np.random.default_rng(rng)
        return getattr(seqHPC, sim_config["dataset_cls"])(config=sim_config, rng=rng)

    def generate_data(self, sim_config):
        return sim_config["dataset_obj"].generate_data(sim_config)

    def launch_simulation(self, sim_config, sim_id):

        print(sim_config)

        sim_config["Ns"] = seqHPC.utils.N_scale_to_N(sim_config["N_scale"])

        sim_config["input_as_ECin"] = getattr(seqHPC, sim_config["dataset_cls"]).input_as_ECin
        sim_config["ECout_as_output"] = getattr(seqHPC, sim_config["dataset_cls"]).ECout_as_output
        sim_config["SIout_activation_func"] = getattr(seqHPC, sim_config["dataset_cls"]).activation_func
        sim_config["loss_fn"] = getattr(seqHPC, sim_config["dataset_cls"]).loss_fn
        sim_config["dim"] = getattr(seqHPC, sim_config["dataset_cls"]).dim

        if sim_config["dim"]=="ECin":
            sim_config["dim"] = sim_config["Ns"]["ECin"]

        sim_config["spars_ratios"] = {
            'ECin': 1 if sim_config["no_kWTA_in_MSP"] else (sim_config["bio_spars"] * .35 + (1-sim_config["bio_spars"]) * 1),
            'DG': sim_config["bio_spars"] * .0078 + (1-sim_config["bio_spars"]) * 1,
            'CA3': sim_config["bio_spars"] * .032 + (1-sim_config["bio_spars"]) * 1,
            'CA1': 1 if sim_config["no_kWTA_in_MSP"] else (sim_config["bio_spars"] * .09 + (1-sim_config["bio_spars"]) * 1),
            'ECout': 1 if sim_config["no_kWTA_in_MSP"] else (sim_config["bio_spars"] * .35 + (1-sim_config["bio_spars"]) * 1),
        }

        sim_config["n_test_tasks"] = int(sim_config["n_test_tasks"])

        sim_config = self.set_simulation_seed(sim_config)

        sim_config["xp_cls"] = self.__class__




        sim_config["dataset_obj"] = self.create_dataset(sim_config, sim_config["rng"])
        sim_config["data"] = self.generate_data(sim_config)


        import time
        path = self.path+'/simulations/sim_'+'{0:07d}'.format(sim_id)+'_' + str(time.time())
        os.makedirs(path)
        with open(path+"/config.pickle", "wb") as f:

            # Most of the time, we do not want to dump the data
            dump_data = sim_config["platform"]=="plafrim"

            pickle.dump({k:v for k,v in sim_config.items() if k not in ["dataset_obj", "data"] or dump_data}, f)

        # For details: https://www.plafrim.fr/
        if sim_config["platform"]=="plafrim":
            from . import plafrim_utils
            self.job_dispatcher.tasks.append(plafrim_utils.Task(self.path+'/code', path))
            return None
        else:
            return self.simulate(path, sim_config)