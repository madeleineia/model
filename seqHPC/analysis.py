import os
import pickle
import pandas as pd
import numpy as np

import sys

import seqHPC.utils


class Analysis:
    def __init__(self, args):
        self.args = args

    @property
    def xp_path(self):
        return "./logs/" + self.args.experiment_id

    @property
    def processed_results_path(self):
        return self.xp_path + "/"+type(self).__name__+".df"

    @property
    def figures_path(self):
        return self.xp_path + "/figures"

    def make_fig(self, fig, ax=None, name=None, show=False, save_png=True, save_pdf=True, close=True, xp_in_filename=True):
        return seqHPC.utils.make_fig(fig, ax, self.figures_path, self.args.experiment_id+"-"+name if xp_in_filename else name, show, save_png, save_pdf, close)

    @property
    def simulations_path(self):
        return self.xp_path+"/simulations"

    def preprocess_sim(self, config, data):

        # Add config parameters to data
        for k, v in config.items():
            data[k] = v

        # data["model_alpha"] = ' '.join([data["model_cls"], str(data["alpha"])])

        # for k in ["true_pos", "encoding_fr", "true_fr", "recall_fr"]:
        #     del data[k]
        # print(data.keys())

        return data


    def preprocess(self):

        never_load = True
        if never_load:
            print("WARNING: never loading saved results")

        save_results = True
        # If dataframe file already created, load it
        if not never_load and os.path.isfile(self.processed_results_path):
            with open(self.processed_results_path, 'rb') as f:
                self.results = pickle.load(f)
            print("Loading processed results")

        else:
            simulation_dirs = [self.simulations_path+'/'+simulation_dir for simulation_dir in os.listdir(self.simulations_path)]
            print(len(simulation_dirs), "dirs")
            self.results = [] # temporary list to store the data to be stored in the dataframe


            for i,d in enumerate(simulation_dirs):

                try:
                    with open(d + "/config.pickle", 'rb') as f:
                        config = pickle.load(f)
                    with open(d + "/results.pickle", 'rb') as f:
                        data = pickle.load(f)

                    data = self.preprocess_sim(config, data)
                    self.results.append(data)

                except IOError:
                    save_results = False
                    print(d + "/config.pickle or " + d + "/results.pickle do not exist. Simulations are probably pending, hence results will not be saved.")





            self.results = pd.DataFrame(self.results)


            if save_results:
                print("Saving processed results")
                pickle.dump(self.results, open(self.processed_results_path, "wb"))

    def analyse(self):
        raise NotImplementedError("Subclasses should implement this!")

    def __call__(self):
        os.makedirs(self.figures_path, exist_ok=True)
        self.preprocess()
        self.analyse()


