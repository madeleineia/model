from seqHPC import Analysis

import seqHPC
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.colors as colors
import matplotlib.cm as cmx

import numpy as np

class DefaultAnalysis(Analysis):

    def preprocess(self):
        super().preprocess()

        # Only one simulation, therefore we don't need a dataframe, we just need a dict
        self.results = self.results.iloc[0].to_dict()



    def analyse(self):

        import pickle
        if self.results["dataset_cls"] in ["AgentInABoxPC", "AgentInABoxGC"]:
            with open("seqHPC/datasets/{dataset_cls}.pickle".format(dataset_cls=self.results['dataset_cls']), 'rb') as f:
                ds_dict = pickle.load(f)

                for k,v in ds_dict.items():
                    if k in ["true_pos", "true_fr"]:
                        v = v[:self.results["n_walk"], :self.results["walk_length"]]
                        assert v.shape[:2]==(self.results["n_walk"],self.results["walk_length"])

                        if k == "true_fr":
                            assert v.shape[2]==self.results["dim"]

                    self.results[k] = v

            fig, axs = plt.subplots(nrows=1, ncols=4)
            show_from = 17
            self.results["dataset"].plot_trajectories(self.results["true_pos"][show_from:,1:,:], fig, axs[0])
            self.results["dataset"].decode_and_plot_trajectories(self.results["true_fr"][show_from:,1:,:], fig, axs[1])
            self.results["dataset"].decode_and_plot_trajectories(self.results["encoding_fr"][show_from:], fig, axs[2])
            self.results["dataset"].decode_and_plot_trajectories(self.results["recall_fr"][show_from:], fig, axs[3])
            self.make_fig(fig, name="trajectories")

        # for criterion in ["pos", "fr"]:
        #
        #     values = list(range(self.results["n_walk"]))
        #     cm = plt.get_cmap('magma')
        #     cNorm = colors.Normalize(vmin=0, vmax=values[-1])
        #     scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cm)
        #
        #     for phase in ["recall"]:  # ["encoding", "recall"]:
        #         for i in range(self.results["n_walk"]):
        #             colorVal = scalarMap.to_rgba(values[i])
        #             plt.plot(self.results["errors"][criterion][phase+"_identity_ratio"][i], color=colorVal, alpha=.7)
        #
        #     plt.ylabel("RMSE t+1 / RMSE t")
        #     plt.xlabel("Walk Step")
        #
        #     plt.legend()
        #
        #     divider = make_axes_locatable(plt.gca())
        #     ax_cb = divider.new_horizontal(size="5%", pad=0.05)
        #     cb1 = mpl.colorbar.ColorbarBase(ax_cb, cmap=cm, orientation='vertical')
        #     plt.gcf().add_axes(ax_cb)
        #
        #     plt.title(criterion+"_ratio")
        #     self.make_fig(plt.gcf(), name=criterion+"_ratio")

        for criterion in ["fr"]:#["pos", "fr"]:

            values = list(range(self.results["n_walk"]))
            cm = plt.get_cmap('magma')
            cNorm = colors.Normalize(vmin=0, vmax=values[-1])
            scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cm)

            for phase in ["recall"]:#["encoding", "recall"]:
                for i in range(self.results["n_walk"]):
                    colorVal = scalarMap.to_rgba(values[i])
                    plt.plot(self.results["errors"][criterion][phase][i], color=colorVal, alpha=.7)

            # plt.plot(self.results["errors"][criterion]["identity"].mean(axis=0), color="black", linestyle="dotted", label="identity")
            plt.plot(self.results["errors"][criterion]["random"].mean(axis=0), color="black", linestyle="dashed", label="random")

            plt.ylabel("Mean Absolute Error")
            plt.xlabel("Walk Step")
            plt.ylim(0,1)

            plt.legend()

            divider = make_axes_locatable(plt.gca())
            ax_cb = divider.new_horizontal(size="5%", pad=0.05)
            cb1 = mpl.colorbar.ColorbarBase(ax_cb, cmap=cm, orientation='vertical')
            plt.gcf().add_axes(ax_cb)

            plt.title(criterion)

            self.make_fig(plt.gcf(), name=criterion)

            # Scatterplot walk id vs perf

            plt.scatter(range(self.results["n_walk"]), self.results["errors"][criterion]["recall"][:,self.results["recall_autonomous_from"]:].mean(axis=1))
            self.make_fig(plt.gcf(), name=criterion+"_walkid")
