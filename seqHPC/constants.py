import seqHPC

NEURON_COUNTS = {
    "ECin": 110000,
    "DG": 1200000,
    "CA3": 250000,
    "CA1": 390000,
    "ECout": 330000}


MELCHIOR_N_CONVERSION = 1 / 100000  # used to match with N from Melchior et al. (2019)

READABLE_TARGET_LABEL = {
    None: "None",
    tuple(["ECin"]): "ECin",
    tuple(["DG"]): "DG",
    tuple(["CA3"]): "CA3",
    tuple(["CA1"]): "CA1",
    tuple(["ECout"]): "ECout",
    tuple(["ECin","CA1"]): "ECin+CA1",
    tuple(["CA1","ECout"]): "CA1+ECout",
    tuple(["ECin","CA1","ECout"]): "Bio+", # Hypothesized biological configuration
    tuple(["ECin","DG","CA1","ECout"]): "Bio", # Hypothesized biological configuration
    tuple(["ECin","DG","CA3","CA1","ECout"]): "All"
}

BIO_INDEX = list(READABLE_TARGET_LABEL.values()).index("Bio")

MODULAR_LAYERS = ["ECin", "DG", "CA3", "CA1", "ECout"]
