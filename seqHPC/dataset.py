import torch


class Dataset:

    input_as_ECin = False
    ECout_as_output = False
    activation_func = torch.nn.Identity()
    loss_fn = torch.nn.MSELoss
    dim = None

    def __init__(self, config, rng):
        self.config = config
        self.rng = rng