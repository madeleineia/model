import argparse
import seqHPC
import os
from pathlib import Path

def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--platform', choices=["cpu", "gpu", "plafrim"], default="cpu", help='Which computing platform to use. See docs for PlaFRIM at https://www.plafrim.fr/')
    parser.add_argument('-m', '--model_cls', help="Name of the model class", required=False)
    parser.add_argument('-c', '--class_to_run', help="Name of the experiment or analysis class to run", required=True)
    parser.add_argument('--dataset_cls', help="Name of the dataset class")
    parser.add_argument('-xpid', '--experiment_id', help='Name of the experiment to analyse if running an analysis, or name to give to the experiment to run.')
    parser.add_argument('-s', '--seed', help="Default seed value", default=None, type=int)

    return parser

def get_last_experiment_id(path="logs/"):
    paths = sorted(Path(path).iterdir(), key=os.path.getmtime)
    return str(paths[-1])[len(path):]

if __name__ == '__main__':

    parser = create_parser()
    args = parser.parse_args()

    import time
    start = time.time()

    # Get class from name
    class_to_run = getattr(seqHPC, args.class_to_run)

    # Create class object
    obj = class_to_run(args)

    if issubclass(class_to_run, seqHPC.Analysis) and args.experiment_id is None:
        args.experiment_id = get_last_experiment_id()
        print("No experiment id given. Using id of last experiment:", args.experiment_id)

    # Run class object
    obj()

    print("Time:", time.time()-start)

